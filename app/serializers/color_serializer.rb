class ColorSerializer < ActiveModel::Serializer
  attributes :id, :name, :color_code
end
